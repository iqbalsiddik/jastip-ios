//
//  SessionManager.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 20/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation

class SessionManager {
    func setUser(user: User) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(encodedData as AnyObject, forKey: "user")
        
        setIsLoggedIn()
    }
    
    private func setIsLoggedIn() {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: true)
        UserDefaults.standard.set(encodedData as AnyObject, forKey: "isLoggedIn")
    }
    
    private func removeuser() {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: User())
        UserDefaults.standard.set(encodedData as AnyObject, forKey: "user")
    }
    
    private func removeLoggedIn() {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: false)
        UserDefaults.standard.set(encodedData as AnyObject, forKey: "isLoggedIn")
    }
    
    func getUser() -> User {
        var user = User()
        if let data = UserDefaults.standard.object(forKey: "user") as? NSData {
            user = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! User
        }
        
        return user
    }
    
    func getIsLoggedIn() -> Bool {
        var isLoggedIn = false
        if let data = UserDefaults.standard.object(forKey: "isLoggedIn") as? NSData {
            isLoggedIn = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! Bool
        }
        
        return isLoggedIn
    }
    
    func logout() {
        removeuser()
        removeLoggedIn()
    }
}

