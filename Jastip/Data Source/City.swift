//
//  City.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 01/09/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import EVReflection
import ObjectMapper

class CityResponse: Mappable {
    var cities: [City]!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        cities <- map["data"]
    }
}

class City: Mappable {
    var reg_id : String!
    var reg_nama: String!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        reg_id <- map["reg_id"]
        reg_nama <- map["reg_nama"]
    }
}
