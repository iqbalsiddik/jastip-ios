//
//  File.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 23/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Order: EVObject {
    var prj_id = String()
    var titipan_id = String()
    var tgl_titip = String()
    var user_username = String()
    var photo = String()
    var status_titipan = String()
    var item = Item()
}

