//
//  Trip.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 20/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Trip : EVObject {
    var prj_id = String()
    var asal = String()
    var tujuan = String()
    var tgl_mulai = String()
    var tgl_selesai = String()
    var photo = String()
    var deskripsi = String()
    var user_username = String()
}

