//
//  Profile.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 14/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Profile: EVObject {
    var namaBlkg = String()
    var namaDepan = String()
    var userSkor = Int()
    var nohp = String()
    var id = String()
    var photoprofile = String()
    var email = String()
    var jmlSkor = String()
}
