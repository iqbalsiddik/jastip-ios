//
//  MyRequest.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 22/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class MyRequest: EVObject {
    var nama_penitip = String()
    var tgl_titip = String()
    var tgl_deadline = String()
    var tujuan = String()
    var nama_barang = String()
    var titipan_id = String()
    var photo = String()
}
