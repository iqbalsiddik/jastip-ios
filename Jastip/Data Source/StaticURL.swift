//
//  URL.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 20/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation

class StaticURL {
    static let FOTO_URL = "http://api.jastip.biz.id/profile"
    static let BASE_URL = "http://api.jastip.biz.id/"

    static let API_BASE_URL = BASE_URL + "api/"
}

