//
//  Detail.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 26/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Detail: EVObject {
    var status_titipan = String()
    var harga_bayar = Double()
    var tgl_mulai = String()
    var tgl_selesai = String()
    var photo = String()
    var kurir = String()
    var tgl_titip = String()
    var tujuan = String()
    var user_perjalanan = String()
    var asal = String()
    var harga = String()
    var user_penitip = String()
    var resi = String()
    var bukti_pembayaran = String()
}
