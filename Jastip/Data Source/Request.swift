//
//  Request.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 23/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Request: EVObject {
    var titipan_id = String()
    var tgl_titip = String()
    var prj_id = String()
    var user_username = String()
    var status = String()
    var created_at = String()
    var item = Item()
}

