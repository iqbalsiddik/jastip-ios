//
//  User.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 20/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class User : EVObject {
    var username = String()
    var photo = String()
    var nama = String()
    var email = String()
    var nohp = String()
    var token = String()
}
