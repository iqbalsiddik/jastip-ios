//
//  Feedback.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 17/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import EVReflection

class Feedback: EVObject {
    var feed = String()
    var nama = String()
    var rating = Int()
    var pada = String()
    var titipan_id = Int()
}
