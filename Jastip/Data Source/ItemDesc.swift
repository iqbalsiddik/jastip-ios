//
//  ItemDesc.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 26/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class ItemDesc: EVObject {
    var nama_barang = String()
    var deskripsi_barang = String()
    var jml = String()
    var satuan = String()
    
}
