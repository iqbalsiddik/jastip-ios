//
//  Data.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 26/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class DataJastip: EVObject {
    var feedback = TripFeedback()
    var detail = Detail()
    var deskripsi = [ItemDesc]()
}
