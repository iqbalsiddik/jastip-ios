//
//  Item.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 24/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import EVReflection

class Item: EVObject {
    var titipan_titipan_id = Int()
    var updated_at = String()
    var jml = Int()
    var satuan = String()
    var created_at = String()
    var nama_barang = String()
    var deskripsi_barang = String()
    var detail_id = Int()
}
