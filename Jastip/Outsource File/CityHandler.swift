//
//  CityHandler.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 01/09/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

class CityHandler {
    var cities = [City]()
    
    func getCityList(completionHandler: @escaping ([City]) -> Void) {
        Alamofire.request(StaticURL.API_BASE_URL + "perjalanan/list-region/" + SessionManager().getUser().token,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers: nil)
            .responseObject { (response: DataResponse<CityResponse>) in
                switch response.result {
                case let .success(data):
                    dump(data)
                    self.cities = data.cities
                    completionHandler(self.cities)
                case let .failure(error):
                    dump(error)
                }
        }
    }
}
