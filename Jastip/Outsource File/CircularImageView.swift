//
//  CircularImageView.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 15/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit

extension UIImageView {
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
