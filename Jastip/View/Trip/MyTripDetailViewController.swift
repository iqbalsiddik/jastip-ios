//
//  MyTripViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 06/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit

class MyTripNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

class MyTripDetailViewController: UIViewController {
    
    var selectedTrip: Trip?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTrip()
    }
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var arrival: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    private func loadTrip() {
        username.text = selectedTrip?.user_username
        origin.text = (selectedTrip?.asal)!// + "\n" + (selectedTrip?.tgl_mulai)!
        arrival.text = (selectedTrip?.tujuan)!// + "\n" + (selectedTrip?.tgl_selesai)!
        desc.text = selectedTrip?.deskripsi
        
        avatar.setRounded()
        avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + SessionManager().getUser().photo)!)
    }
    

    @IBAction func backToList() {
        self.dismiss(animated: true, completion: nil)
    }

}
