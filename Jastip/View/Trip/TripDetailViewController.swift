//
//  TripViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 07/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import Toast_Swift

class TripNavViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

class TripDetailViewController: UIViewController {

    var selectedTrip: Trip?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTrip()
    }
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var arrival: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var avatar2: UIImageView!
    
    
    private func loadTrip() {
        username.text = selectedTrip?.user_username
        origin.text = selectedTrip?.asal
        arrival.text = selectedTrip?.tujuan
        desc.text = selectedTrip?.deskripsi
        
        avatar.setRounded()
        avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + selectedTrip!.photo)!)
        avatar2.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + selectedTrip!.photo)!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination is TripRequestViewController) {
            let destinationVc = segue.destination as! TripRequestViewController
            destinationVc.selectedTrip = self.selectedTrip
        }
    }
    
    @IBAction func backToList() {
        self.dismiss(animated: true, completion: nil)
    }
}

class TripRequestViewController: UIViewController, NVActivityIndicatorViewable {
    
    var selectedTrip: Trip?
    let date = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTrip()
    }
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var arrival: UILabel!
    @IBOutlet weak var departDate: UILabel!
    
    @IBOutlet weak var itemName: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var unit: UITextField!
    @IBOutlet weak var descriptionOrder: UITextField!
    
    private func loadTrip() {
        username.text = selectedTrip?.user_username
        origin.text = "Depart from " + (selectedTrip?.asal)!
        arrival.text = "Arrive at " + (selectedTrip?.tujuan)! + " " + (selectedTrip?.tgl_selesai)!
        departDate.text = selectedTrip?.tgl_mulai
    }
    
    @IBAction func order() {
        formatter.dateFormat = "yyyy-MM-dd"
        let orderDate = formatter.string(from: date)
        
        doOrder(date: orderDate)
    }
    
    private func doOrder(date: String) {
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/create",
                          method: .post,
                          parameters: ["token": SessionManager().getUser().token,
                                       "tgl_titip": date,
                                       "prj_id": selectedTrip!.prj_id,
                                       "status": "1",
                                       "user_username": SessionManager().getUser().username,
                                       "nama_barang": itemName.text!,
                                       "jml": amount.text!,
                                       "satuan": unit.text!,
                                       "deskripsi_barang": descriptionOrder.text!] as Parameters,
                          encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                if (response.result.value != nil) {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.view.makeToast("Order success!", duration: 2.0, position: .bottom)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.dismiss(animated: true, completion: nil)
                        }
                    } else {
                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                    }
                } else {
                    self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                    print(response)
                }
                
                self.view.hideToastActivity()
        }
    }
}
