//
//  AddTripViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 27/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class AddTripnavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

class AddTripViewController: UIViewController {
    
    let formatter = DateFormatter()
    
    @IBOutlet weak var origin: UITextField!
    @IBOutlet weak var destination: UITextField!
    @IBOutlet weak var depart: UITextField!
    @IBOutlet weak var arrive: UITextField!
    @IBOutlet weak var desc: UITextField!
    @IBOutlet weak var arriveDatePicker: UIDatePicker!
    @IBOutlet weak var departDatePicker: UIDatePicker!
    
    var cityList = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            CityHandler().getCityList() { cities in
                self.cityList = cities
            }
        }
    }
    
    @IBAction func backToList() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func arriveDatePickerChanged(_ sender: Any) {
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let strDate = formatter.string(from: arriveDatePicker.date)
        arrive.text = strDate
    }
    
    @IBAction func departDatePickerChanged(_ sender: Any) {
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let strDate = formatter.string(from: departDatePicker.date)
        depart.text = strDate
    }
    
    @IBAction func create() {
        if getCityId(cityName: origin.text!) == nil {
            self.view.makeToast("Can't find origin city!")
            self.view.hideToastActivity()
        } else if getCityId(cityName: destination.text!) == nil {
            self.view.makeToast("Can't find origin city!")
            self.view.hideToastActivity()
        } else {
            let param = [
                "asal": getCityId(cityName: origin.text!)!,
                "tujuan": getCityId(cityName: destination.text!)!,
                "tgl_mulai": depart.text!,
                "tgl_selesai": arrive.text!,
                "deskripsi": desc.text!,
                "user_username": SessionManager().getUser().username,
                "token": SessionManager().getUser().token
                ] as Parameters
            
            Alamofire.request(StaticURL.API_BASE_URL + "perjalanan/create",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.view.makeToast("Trip saved!")
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
                                
            }
        }
    }
    
    private func getCityId(cityName: String) -> String? {
        for city in cityList {
            if cityName == city.reg_nama {
                return city.reg_id
            }
        }
        
        return nil
    }
}
