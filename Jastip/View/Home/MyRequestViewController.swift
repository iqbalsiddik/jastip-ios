//
//  MyRequestViewModel.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 23/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON

class MyRequestViewController: UITableViewController, IndicatorInfoProvider {
    private var indexCount = 0
    private var myRequestList = [Request]()
    private let refreshControllTV = UIRefreshControl()
    private let user = SessionManager().getUser()
    
    private var floatingButton: UIButton?
    // TODO: Replace image name with your own image:
    private let floatingButtonImageName = "avatar"
    private static let buttonHeight: CGFloat = 50.0
    private static let buttonWidth: CGFloat = 50.0
    private let roundValue = MyRequestViewController.buttonHeight/2
    private let trailingValue: CGFloat = 15.0
    private let bottomValue: CGFloat = 75.0
    private let leadingValue: CGFloat = 15.0
    private let shadowRadius: CGFloat = 2.0
    private let shadowOpacity: Float = 0.5
    private let shadowOffset = CGSize(width: 0.0, height: 5.0)
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createFloatingButton()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        guard floatingButton?.superview != nil else {  return }
        DispatchQueue.main.async {
            self.floatingButton?.removeFromSuperview()
            self.floatingButton = nil
        }
        super.viewWillDisappear(animated)
    }
    
    private func createFloatingButton() {
        floatingButton = UIButton(type: .custom)
        floatingButton?.translatesAutoresizingMaskIntoConstraints = false
        floatingButton?.backgroundColor = .white
        floatingButton?.setImage(UIImage(named: floatingButtonImageName), for: .normal)
        floatingButton?.addTarget(self, action: #selector(doThisWhenButtonIsTapped(_:)), for: .touchUpInside)
        constrainFloatingButtonToWindow()
        makeFloatingButtonRound()
        addShadowToFloatingButton()
    }
    
    // TODO: Add some logic for when the button is tapped.
    @IBAction private func doThisWhenButtonIsTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddRequest", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddRequestNavViewController") as! AddRequestNavViewController
        
        present(viewController, animated: true, completion: nil)
    }
    
    private func constrainFloatingButtonToWindow() {
        DispatchQueue.main.async {
            guard let keyWindow = UIApplication.shared.keyWindow,
                let floatingButton = self.floatingButton else { return }
            keyWindow.addSubview(floatingButton)
            keyWindow.trailingAnchor.constraint(equalTo: floatingButton.trailingAnchor,
                                                constant: self.trailingValue).isActive = true
            keyWindow.bottomAnchor.constraint(equalTo: floatingButton.bottomAnchor,
                                              constant: self.bottomValue).isActive = true
            floatingButton.widthAnchor.constraint(equalToConstant:
                MyRequestViewController.buttonWidth).isActive = true
            floatingButton.heightAnchor.constraint(equalToConstant:
                MyRequestViewController.buttonHeight).isActive = true
        }
    }
    
    private func makeFloatingButtonRound() {
        floatingButton?.layer.cornerRadius = roundValue
    }
    
    private func addShadowToFloatingButton() {
        floatingButton?.layer.shadowColor = UIColor.black.cgColor
        floatingButton?.layer.shadowOffset = shadowOffset
        floatingButton?.layer.masksToBounds = false
        floatingButton?.layer.shadowRadius = shadowRadius
        floatingButton?.layer.shadowOpacity = shadowOpacity
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControllTV
        } else {
            tableView.addSubview(refreshControllTV)
        }
        refreshControllTV.addTarget(self, action: #selector(refreshRequestData(_:)), for: .valueChanged)
        
        getMyRequestList()
    }
    
    @objc private func refreshRequestData(_ sender: Any) {
        myRequestList.removeAll()
        indexCount = 0
        self.tableView.reloadData()
        
        getMyRequestList()
    }
    
    private func getMyRequestList() {
        self.view.makeToastActivity(.center)
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/list-saya/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if (response.result.value != nil) {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.indexCount = json["data"].count
                        
                        var newIndex: Int
                        if self.indexCount == 0 {
                            newIndex = 0
                        } else {
                            newIndex = self.indexCount - 1
                        }
                        
                        for i in (0...newIndex).reversed() {
                            self.handleJSON(json: json["data"][i])
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                } else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription ?? "Something went wrong")
                }
        }
    }
    
    private func handleJSON(json: JSON) {
        let request = Request(json: json.description)
        myRequestList.append(request)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControllTV.endRefreshing()
        }
        
        self.view.hideToastActivity()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "My Request")
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRequestCellView", for: indexPath) as! MyRequestCellView
        
        cell.usernameLabel.text = self.myRequestList[indexPath.row].user_username
        cell.statusLabel.text = self.myRequestList[indexPath.row].status
        cell.dateLabel.text = self.myRequestList[indexPath.row].tgl_titip
        cell.itemLabel.text = self.myRequestList[indexPath.row].item.nama_barang + " " + String(self.myRequestList[indexPath.row].item.jml) + " " +
            self.myRequestList[indexPath.row].item.satuan
        
        cell.avatar.setRounded()
        cell.avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + user.photo)!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Request", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RequestNavViewController") as! RequestNavViewController
        let requestHandlerViewController = viewController.viewControllers.first as! RequestHandlerViewController
        requestHandlerViewController.tripId = self.myRequestList[indexPath.row].titipan_id
        
        present(viewController, animated: true, completion: nil)
    }
}

class MyRequestCellView: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

