//
//  ProfileViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 12/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadProfile()
    }
    
    private func loadProfile() {
        let token = SessionManager().getUser().token
        self.view.makeToastActivity(.center)
        
        Alamofire.request(StaticURL.API_BASE_URL + "user/detail/" + token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if (response.result.value != nil) {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        let profile = Profile(json: json["data"].description)
                        self.showProfile(profile: profile)
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                } else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription)
                }
        }
    }
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var avatar: UIImageView!
    
    private func showProfile(profile: Profile) {
        username.text = profile.namaDepan + " " + profile.namaBlkg
        reviewCount.text = String(profile.userSkor) + " reviews"
        ratingStar.rating = Double(profile.jmlSkor)!
        avatar.setRounded()
        avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + profile.photoprofile)!)
        
        self.view.hideToastActivity()
    }
    
    @IBAction func gotoFeedbackAsJastiper() {
        let storyboard = UIStoryboard(name: "Feedback", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FeedbackNavViewController") as! FeedbackNavViewController
        let feedbackVC = viewController.viewControllers.first as! FeedbackViewController
        feedbackVC.feedbackType = "jastiper"
        
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func gotoFeedbackAsAgent() {
        let storyboard = UIStoryboard(name: "Feedback", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FeedbackNavViewController") as! FeedbackNavViewController
        let feedbackVC = viewController.viewControllers.first as! FeedbackViewController
        feedbackVC.feedbackType = "agent"
        
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func gotoEditProfile() {
        let storyboard = UIStoryboard(name: "EditProfile", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfileNavViewController") as! EditProfileNavViewController
        
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func gotoLogin() {
        SessionManager().logout()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.show(vc, sender: self)
    }
}
