//
//  MyTripViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 20/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class MyTripViewController: UITableViewController {
    private var indexCount = 0
    private var tripList = [Trip]()
    private var selectedTrip: Trip?
    private let refreshControllTV = UIRefreshControl()
    
    private var floatingButton: UIButton?
    // TODO: Replace image name with your own image:
    private let floatingButtonImageName = "avatar"
    private static let buttonHeight: CGFloat = 50.0
    private static let buttonWidth: CGFloat = 50.0
    private let roundValue = MyTripViewController.buttonHeight/2
    private let trailingValue: CGFloat = 15.0
    private let bottomValue: CGFloat = 75.0
    private let leadingValue: CGFloat = 15.0
    private let shadowRadius: CGFloat = 2.0
    private let shadowOpacity: Float = 0.5
    private let shadowOffset = CGSize(width: 0.0, height: 5.0)
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createFloatingButton()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        guard floatingButton?.superview != nil else {  return }
        DispatchQueue.main.async {
            self.floatingButton?.removeFromSuperview()
            self.floatingButton = nil
        }
        super.viewWillDisappear(animated)
    }
    
    private func createFloatingButton() {
        floatingButton = UIButton(type: .custom)
        floatingButton?.translatesAutoresizingMaskIntoConstraints = false
        floatingButton?.backgroundColor = .white
        floatingButton?.setImage(UIImage(named: floatingButtonImageName), for: .normal)
        floatingButton?.addTarget(self, action: #selector(doThisWhenButtonIsTapped(_:)), for: .touchUpInside)
        constrainFloatingButtonToWindow()
        makeFloatingButtonRound()
        addShadowToFloatingButton()
    }
    
    @IBAction private func doThisWhenButtonIsTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddTrip", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddTripnavViewController") as! AddTripnavViewController
        
        present(viewController, animated: true, completion: nil)
    }
    
    private func constrainFloatingButtonToWindow() {
        DispatchQueue.main.async {
            guard let keyWindow = UIApplication.shared.keyWindow,
                let floatingButton = self.floatingButton else { return }
            keyWindow.addSubview(floatingButton)
            keyWindow.trailingAnchor.constraint(equalTo: floatingButton.trailingAnchor,
                                                constant: self.trailingValue).isActive = true
            keyWindow.bottomAnchor.constraint(equalTo: floatingButton.bottomAnchor,
                                              constant: self.bottomValue).isActive = true
            floatingButton.widthAnchor.constraint(equalToConstant:
                MyTripViewController.buttonWidth).isActive = true
            floatingButton.heightAnchor.constraint(equalToConstant:
                MyTripViewController.buttonHeight).isActive = true
        }
    }
    
    private func makeFloatingButtonRound() {
        floatingButton?.layer.cornerRadius = roundValue
    }
    
    private func addShadowToFloatingButton() {
        floatingButton?.layer.shadowColor = UIColor.black.cgColor
        floatingButton?.layer.shadowOffset = shadowOffset
        floatingButton?.layer.masksToBounds = false
        floatingButton?.layer.shadowRadius = shadowRadius
        floatingButton?.layer.shadowOpacity = shadowOpacity
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControllTV
        } else {
            tableView.addSubview(refreshControllTV)
        }
        refreshControllTV.addTarget(self, action: #selector(refreshRequestData(_:)), for: .valueChanged)
        
        getMyTripList()
    }
    
    @objc private func refreshRequestData(_ sender: Any) {
        tripList.removeAll()
        indexCount = 0
        self.tableView.reloadData()
        
        getMyTripList()
    }
    
    private func getMyTripList() {
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "perjalanan/list-user/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if (response.result.value != nil) {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.indexCount = json["data"].count
                        
                        var newIndex: Int
                        if self.indexCount == 0 {
                            newIndex = 0
                        } else {
                            newIndex = self.indexCount - 1
                        }
                        
                        for i in (0...newIndex).reversed() {
                            self.handleJSON(json: json["data"][i])
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                } else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription)
                }
        }
    }
    
    private func handleJSON(json: JSON) {
        let trip = Trip(json: json.description)
        tripList.append(trip)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControllTV.endRefreshing()
        }
        
        self.view.hideToastActivity()
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myTripCellView", for: indexPath) as! MyTripCellView
        
        cell.deperatureLabel.text = self.tripList[indexPath.row].asal
        cell.arriveLabel.text = self.tripList[indexPath.row].tujuan
        cell.deperatureDateLabel.text = self.tripList[indexPath.row].tgl_mulai
        cell.arriveDateLabel.text = self.tripList[indexPath.row].tgl_selesai
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTrip = self.tripList[indexPath.row]
        let storyboard = UIStoryboard(name: "My Trip", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyTripNavController") as! MyTripNavController
        let tripDetailVC = viewController.viewControllers.first as! MyTripDetailViewController
        tripDetailVC.selectedTrip = self.tripList[indexPath.row]

        present(viewController, animated: true, completion: nil)
    }
}

class MyTripCellView: UITableViewCell {
    
    @IBOutlet weak var deperatureLabel: UILabel!
    @IBOutlet weak var arriveLabel: UILabel!
    @IBOutlet weak var arriveDateLabel: UILabel!
    @IBOutlet weak var deperatureDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

