//
//  RequestViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 22/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class RequestViewController: UITableViewController, IndicatorInfoProvider {
    private var indexCount = 0
    private var requestList = [MyRequest]()
    private let refreshControllTV = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControllTV
        } else {
            tableView.addSubview(refreshControllTV)
        }
        refreshControllTV.addTarget(self, action: #selector(refreshRequestData(_:)), for: .valueChanged)
        
        getRequestList()
    }
    
    @objc private func refreshRequestData(_ sender: Any) {
        requestList.removeAll()
        indexCount = 0
        self.tableView.reloadData()
        
        getRequestList()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Request")
    }
    
    private func getRequestList() {
        self.view.makeToastActivity(.center)
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/list-request/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if response.result.value != nil {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.indexCount = json["data"].count
                        
                        var newIndex: Int
                        if self.indexCount == 0 {
                            newIndex = 0
                        } else {
                            newIndex = self.indexCount - 1
                        }
                        
                        for i in (0...newIndex).reversed() {
                            self.handleJSON(json: json["data"][i])
                        }
                        
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                }  else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription)
                }
                
        }
    }
    
    private func handleJSON(json: JSON) {
        let request = MyRequest(json: json.description)
        requestList.append(request)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControllTV.endRefreshing()
        }
        
        self.view.hideToastActivity()
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCellView", for: indexPath) as! RequestCellView
        
        cell.nameLabel.text = self.requestList[indexPath.row].nama_penitip
        cell.destinationLabel.text = "Destination: " + self.requestList[indexPath.row].tujuan
        cell.descriptionLabel.text = self.requestList[indexPath.row].nama_barang
        cell.deparatureLabel.text = self.requestList[indexPath.row].tgl_deadline
        
        cell.avatar.setRounded()
        cell.avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + self.requestList[indexPath.row].photo)!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Request Detail", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RequestDetailNavViewController") as! RequestDetailNavViewController
        let requestDetailVC = viewController.viewControllers.first as! RequestDetailViewController
        requestDetailVC.tripId = self.requestList[indexPath.row].titipan_id

        present(viewController, animated: true, completion: nil)
    }
}

class RequestCellView : UITableViewCell {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var deparatureLabel: UILabel!
}

