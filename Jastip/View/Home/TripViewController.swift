//
//  TripViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 22/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON
import Toast_Swift

class TripViewController: UITableViewController, IndicatorInfoProvider {
    private var indexCount = 0
    private var tripList = [Trip]()
    private let refreshControllTV = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControllTV
        } else {
            tableView.addSubview(refreshControllTV)
        }
        refreshControllTV.addTarget(self, action: #selector(refreshTripData(_:)), for: .valueChanged)
        
        getRequestList()
    }
    
    @objc private func refreshTripData(_ sender: Any) {
        tripList.removeAll()
        indexCount = 0
        self.tableView.reloadData()
        
        getRequestList()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Trip")
    }
    
    private func getRequestList() {
        self.view.makeToastActivity(.center)
        
        Alamofire.request(StaticURL.API_BASE_URL + "perjalanan/list-all/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if response.result.value != nil {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.indexCount = json["data"].count
                        
                        var newIndex: Int
                        if self.indexCount == 0 {
                            newIndex = 0
                        } else {
                            newIndex = self.indexCount - 1
                        }
                        
                        for i in (0...newIndex).reversed() {
                            self.handleJSON(json: json["data"][i])
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                } else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription)
                }
        }
    }
    
    private func handleJSON(json: JSON) {
        let trip = Trip(json: json.description)
        tripList.append(trip)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControllTV.endRefreshing()
        }
        
        self.view.hideToastActivity()
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCellView", for: indexPath) as! TripCellView
        
        cell.nameLabel.text = self.tripList[indexPath.row].user_username
        cell.destinationLabel.text = "Arrive at " + self.tripList[indexPath.row].tujuan
        cell.arriveDate.text = self.tripList[indexPath.row].tgl_selesai
        cell.originLabel.text = "Depart from " + self.tripList[indexPath.row].asal
        cell.deperatureDate.text = self.tripList[indexPath.row].tgl_mulai
        
        cell.avatar.setRounded()
        cell.avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + self.tripList[indexPath.row].photo)!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Trip", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TripNavViewController") as! TripNavViewController
        let tripDetailVC = viewController.viewControllers.first as! TripDetailViewController
        tripDetailVC.selectedTrip = self.tripList[indexPath.row]
        
        present(viewController, animated: true, completion: nil)
    }
}

class TripCellView: UITableViewCell {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var arriveDate: UILabel!
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var deperatureDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
