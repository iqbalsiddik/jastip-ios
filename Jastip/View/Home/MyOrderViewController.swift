//
//  MyOrderViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 23/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON

class MyOrderViewController: UITableViewController, IndicatorInfoProvider {
    private var indexCount = 0
    private var myOrderList = [Order]()
    private let refreshControllTV = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControllTV
        } else {
            tableView.addSubview(refreshControllTV)
        }
        refreshControllTV.addTarget(self, action: #selector(refreshRequestData(_:)), for: .valueChanged)
        
        getMyOrderList()
    }
    
    @objc private func refreshRequestData(_ sender: Any) {
        myOrderList.removeAll()
        indexCount = 0
        self.tableView.reloadData()
        
        getMyOrderList()
    }
    
    private func getMyOrderList() {
        self.view.makeToastActivity(.center)
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/list-user-lain/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                if (response.result.value != nil) {
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.indexCount = json["data"].count
                        
                        var newIndex: Int
                        if self.indexCount == 0 {
                            newIndex = 0
                        } else {
                            newIndex = self.indexCount - 1
                        }
                        
                        for i in (0...newIndex).reversed() {
                            self.handleJSON(json: json["data"][i])
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
                } else {
                    self.view.hideToastActivity()
                    self.view.makeToast(response.error?.localizedDescription ?? "Something went wrong")
                }
        }
    }
    
    private func handleJSON(json: JSON) {
        let order = Order(json: json.description)
        myOrderList.append(order)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControllTV.endRefreshing()
        }
        
        self.view.hideToastActivity()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "My Order")
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCellView", for: indexPath) as! MyJastipCellView
        
        cell.usernameLabel.text = self.myOrderList[indexPath.row].user_username
        cell.statusLabel.text = self.myOrderList[indexPath.row].status_titipan
        cell.dateLabel.text = self.myOrderList[indexPath.row].tgl_titip
        cell.itemLabel.text = self.myOrderList[indexPath.row].item.nama_barang + " " + String(self.myOrderList[indexPath.row].item.jml) + " " +
            self.myOrderList[indexPath.row].item.satuan
        
        cell.avatar.setRounded()
        cell.avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + self.myOrderList[indexPath.row].photo)!)
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Order", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OrderNavViewController") as! OrderNavViewController
        let orderHandlerViewController = viewController.viewControllers.first as! OrderHandlerViewController
        orderHandlerViewController.tripId = self.myOrderList[indexPath.row].titipan_id

        present(viewController, animated: true, completion: nil)
    }
}

class MyJastipCellView: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
