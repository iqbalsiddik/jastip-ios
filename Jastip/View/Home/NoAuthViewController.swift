//
//  NoAuthViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 16/09/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit

class NoAuthViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func gotoLogin() {
        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        present(viewController, animated: true, completion: nil)
    }

}
