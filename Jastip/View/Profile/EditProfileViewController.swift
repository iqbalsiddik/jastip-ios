//
//  EditProfileViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 17/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class EditProfileNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
}

class EditProfileViewController: UIViewController {
    
    var profile = SessionManager().getUser()
    var avatarStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        loadInitialData()
    }
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var confirmPass: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
    private func setupView() {
        avatar.setRounded()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showImagePicker(sender:)))
        avatar.addGestureRecognizer(tap)
        avatar.isUserInteractionEnabled = true
    }
    
    var imagePicker = UIImagePickerController()
    
    @objc func showImagePicker(sender: AnyObject) {
        ImagePickerManager().pickImage(self) { image in
            self.avatarStr = "data:image/png;base64,[" + (image.compressTo(1)!.toBase64())! + "]"
            self.avatar.image = image
        }
    }
    
    private func loadInitialData() {
        avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + profile.photo)!)
        firstName.text = profile.nama.components(separatedBy: " ").first
        lastName.text = profile.nama.components(separatedBy: " ")[1]
        email.text = profile.email
        phoneNumber.text = profile.nohp
        
        avatarStr = "data:image/png;base64,[" + (avatar.image?.toBase64())! + "]"
    }
    
    private func checkValue() -> Bool {
        if (avatarStr == nil) {
           self.view.makeToast("Add your avatar first!")
            return false
        }
        
        if (firstName.text!.isEmpty) {
            self.view.makeToast("First name can't be empty!")
            return false
        }
        
        if (lastName.text!.isEmpty) {
            self.view.makeToast("Last name can't be empty!")
            return false
        }
        
        if (email.text!.isEmpty) {
            self.view.makeToast("Email can't be empty!")
            return false
        }
        
        if (!email.text!.isValidEmail()) {
            self.view.makeToast("Please input a valid email!")
            return false
        }
        
        if (pass.text!.isEmpty) {
            self.view.makeToast("Password can't be empty!")
            return false
        }
        
        if (confirmPass.text!.isEmpty) {
            self.view.makeToast("Confirm password can't be empty!")
            return false
        }
        
        if (phoneNumber.text!.isEmpty) {
            self.view.makeToast("Phone number can't be empty!")
            return false
        }
        
        if (pass.text != confirmPass.text) {
            self.view.makeToast("Password must be the same with confirm password")
            return false
        }
        
        return true
    }
    
    @IBAction func editProfile(_ sender: Any) {
        if (checkValue()) {
            self.view.makeToastActivity(.center)
            
            Alamofire.request(StaticURL.API_BASE_URL + "user/edit-profile",
                              method: .post,
                              parameters: ["username": profile.username,
                                           "password": pass.text!,
                                           "email": email.text!,
                                           "nama_depan": firstName.text!,
                                           "nama_blkg": lastName.text!,
                                           "nohp": phoneNumber.text!,
                                           "photo": avatarStr!,
                                           "token": profile.token] as Parameters,
                              encoding: JSONEncoding.default)
                .responseJSON { (response) -> Void in
                    if response.result.value != nil {
                        let json = JSON(response.result.value!)
                        if json["status_code"].intValue == 200 {
                            self.doLogin()
                        } else {
                            self.view.makeToast(json["message"].description)
                            self.view.hideToastActivity()
                        }
                    } else {
                        self.view.makeToast(response.error?.localizedDescription)
                        self.view.hideToastActivity()
                    }
            }
        }
    }
    
    private func doLogin() {
        Alamofire.request(StaticURL.API_BASE_URL + "login",
                          method: .post,
                          parameters: ["username": profile.username,
                                       "password": pass.text!],
                          encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                let json = JSON(response.result.value!)
                if json["status_code"].intValue == 200 {
                    let user = User(json: json["data"].description)
                    SessionManager().logout()
                    SessionManager().setUser(user: user)
                    
                    self.view.hideToastActivity()
                    self.view.makeToast("Edit profile success!", duration: 2.0, position: .bottom)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    self.view.makeToast(json["message"].description)
                    self.view.hideToastActivity()
                }
        }
    }
    
    @IBAction func backToProfile(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
