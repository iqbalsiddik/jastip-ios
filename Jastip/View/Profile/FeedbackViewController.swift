//
//  FeedbackNavViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 17/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class FeedbackNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

class FeedbackViewController: UITableViewController {
    
    var feedbackType: String?
    var indexCount = 0
    var feedbackList = [Feedback]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        loadFeedback()
    }
    
    @IBAction func backToProfile(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func loadFeedback() {
        self.view.makeToastActivity(.center)
        if (feedbackType == "jastiper") {
            loadAsJastiper()
            self.navigationItem.title = "Feedback as Jastiper"
        } else if (feedbackType == "agent") {
            loadAsAgent()
            self.navigationItem.title = "Feedback as Agent"
        }
    }
    
    private func loadAsJastiper() {
        Alamofire.request(StaticURL.API_BASE_URL + "user/feedback/penitip/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                let json = JSON(response.result.value!)
                if json["status_code"].intValue == 200 {
                    self.indexCount = json["data"].count
                    
                    for i in (0...self.indexCount) {
                        self.handleJSON(json: json["data"][i])
                    }
                } else {
                    print(json["message"])
                    self.view.hideToastActivity()
                }
        }
    }
    
    private func loadAsAgent() {
        Alamofire.request(StaticURL.API_BASE_URL + "user/feedback/pembawa/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                let json = JSON(response.result.value!)
                if json["status_code"].intValue == 200 {
                    self.indexCount = json["data"].count
                    
                    for i in 0...self.indexCount {
                        self.handleJSON(json: json["data"][i])
                    }
                } else {
                    print(json["message"])
                    self.view.hideToastActivity()
                }
        }
    }
    
    private func handleJSON(json: JSON) {
        let feedback = Feedback(json: json.description)
        feedbackList.append(feedback)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        self.view.hideToastActivity()
    }
    
    //MARK: - TableView Method
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedbackCell", for: indexPath) as! FeedbackCell
        
        cell.name.text = self.feedbackList[indexPath.row].nama
        cell.message.text = self.feedbackList[indexPath.row].feed
        cell.date.text = self.feedbackList[indexPath.row].pada
        cell.rating.rating = Double(self.feedbackList[indexPath.row].rating)
        
        return cell
    }
}

class FeedbackCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


