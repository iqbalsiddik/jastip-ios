//
//  AddRequestNavViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 27/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class AddRequestNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

class AddRequestViewController: UIViewController {

    @IBOutlet weak var origin: UITextField!
    @IBOutlet weak var destination: UITextField!
    @IBOutlet weak var deadline: UITextField!
    @IBOutlet weak var itemName: UITextField!
    @IBOutlet weak var unit: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var desc: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    let date = Date()
    let formatter = DateFormatter()
    var cityList = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            CityHandler().getCityList() { cities in
                self.cityList = cities
            }
        }
    }
    
    @IBAction func backToList() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func getTodayDate() -> String {
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let todayDate = formatter.string(from: date)
        
        return todayDate
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let strDate = formatter.string(from: datePicker.date)
        deadline.text = strDate
    }
    
    @IBAction func create() {
        self.view.makeToastActivity(.center)
        
        if getCityId(cityName: origin.text!) == nil {
            self.view.makeToast("Can't find origin city!")
            self.view.hideToastActivity()
        } else if getCityId(cityName: destination.text!) == nil {
            self.view.makeToast("Can't find origin city!")
            self.view.hideToastActivity()
        } else {
            let param = [
                "nama_barang": itemName.text!,
                "jml": amount.text!,
                "satuan": unit.text!,
                "deskripsi_barang": desc.text!,
                "tgl_titip": getTodayDate(),
                "token": SessionManager().getUser().token,
                "asal": getCityId(cityName: origin.text!)!,
                "tujuan": getCityId(cityName: destination.text!)!,
                "tgl_mulai": deadline.text!,
                "tgl_selesai": deadline.text!,
                "deskripsi": desc.text!,
                "user_username": SessionManager().getUser().username
            ] as Parameters
            
            print(param)
            
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/create-request",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.view.makeToast("Request saved!")
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
            }
        }
    }
    
    private func getCityId(cityName: String) -> String? {
        for city in cityList {
            if cityName == city.reg_nama {
                return city.reg_id
            }
        }
        
        return nil
    }
}
