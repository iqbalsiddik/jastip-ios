//
//  RequestStepViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 12/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import SDWebImage
import SDWebImageWebPCoder

class RequestNavViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

class RequestHandlerViewController: UIViewController {
    
    var tripId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkRequest()
    }
    
    private func checkRequest() {
        print(tripId)
        
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/detail/" + tripId + "/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    let loadedOrder = DataJastip(json: json["data"].description)
                                    
                                    self.setupView(loadedRequest: loadedOrder)
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                }
                            } else {
                                self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                print(response)
                            }
                            
        }
    }
    
    private func setupView(loadedRequest: DataJastip) {
        let storyboard = UIStoryboard.init(name: "Request", bundle: Bundle.main)
        let status = loadedRequest.detail.status_titipan

        print(status)

        if (status == "Unprocessed") {
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestStepOneViewController") as! RequestStepOneViewController
            vc.requestData = loadedRequest
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }

        if (status == "Waiting Price Confirmation" || status == "Confirmed, Waiting For Payment") {
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestStepTwoViewController") as! RequestStepTwoViewController
            vc.requestData = loadedRequest
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }

        if (status == "Paid, Waiting For Validation" || status == "Validated" || status == "Item Is Being Taken") {
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestStepThreeViewController") as! RequestStepThreeViewController
            vc.requestData = loadedRequest
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }

        if (status == "Item Is Being Taken" || status == "Item Has Been Taken" || status == "Item Is Being Delivered") {
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestStepFourViewController") as! RequestStepFourViewController
            vc.requestData = loadedRequest
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (status == "Item Has Been Delivered" || status == "Wrong Item" || status == "Broken Item" || status == "Transaction canceled") {
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestStepFiveViewController") as! RequestStepFiveViewController
            
            vc.requestData = loadedRequest
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class RequestStepOneViewController: UIViewController {
    
    var requestData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (requestData?.deskripsi[0].nama_barang)! + " " + (requestData?.deskripsi[0].jml)! + " " + (requestData?.deskripsi[0].satuan)!
        
        username.text = requestData?.detail.user_penitip
        date.text = requestData?.detail.tgl_titip
        origin.text = requestData?.detail.asal
        destination.text = requestData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = requestData?.deskripsi[0].deskripsi_barang
        price.text = requestData?.detail.harga ?? "-"
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
}

class RequestStepTwoViewController: UIViewController {
    
    var requestData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var paymentMethod: UIView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        if (requestData?.detail.status_titipan != "Waiting Price Confirmation") {
            self.confirmButton.isHidden = true
            self.cancelButton.isHidden = true
            self.paymentMethod.isHidden = false
            self.uploadButton.isHidden = false
        } else {
            self.confirmButton.isHidden = false
            self.cancelButton.isHidden = false
            self.paymentMethod.isHidden = true
            self.uploadButton.isHidden = true
        }
        
        let namaBarang = (requestData?.deskripsi[0].nama_barang)! + " " + (requestData?.deskripsi[0].jml)! + " " + (requestData?.deskripsi[0].satuan)!
        
        username.text = requestData?.detail.user_penitip
        date.text = requestData?.detail.tgl_titip
        origin.text = requestData?.detail.asal
        destination.text = requestData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = requestData?.deskripsi[0].deskripsi_barang
        price.text = requestData?.detail.harga ?? "-"
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTransaction(_ sender: Any) {
        self.view.makeToastActivity(.center)
        
        let param = [
            "titipan_id": tripId!,
            "token": SessionManager().getUser().token,
            ] as Parameters
        
        print(param)
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/pembatalan",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    self.view.makeToast("Cancelled")
                                    self.dismiss(animated: true, completion: nil)
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                }
                            } else {
                                self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                self.view.hideToastActivity()
                                print(response)
                            }
                            
        }
    }
    
    
    @IBAction func confirm(_ sender: Any) {
        let param = [
            "titipan_id": tripId!,
            "token": SessionManager().getUser().token
        ] as Parameters
        
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/konfirmasi-harga",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Price confirmed")
                                    
                                    self.confirmButton.isHidden = true
                                    self.cancelButton.isHidden = true
                                    self.paymentMethod.isHidden = false
                                    self.uploadButton.isHidden = false
                                    
                                } else {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                }
                            } else {
                                self.view.hideToastActivity()
                                self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                print(response)
                            }
                            
        }
    }
    
    var imagePicker = UIImagePickerController()
    var proofBase64: String = ""
    
    @IBAction func uploadProof(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.proofBase64 = "data:image/png;base64,[" + (image.compressTo(1)!.toBase64())! + "]"
            
            let param = [
                "titipan_id": self.tripId!,
                "token": SessionManager().getUser().token,
                "bukti_pembayaran": self.proofBase64
                ] as Parameters
            
            self.view.makeToastActivity(.center)
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/upload-bukti",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        self.view.hideToastActivity()
                                        self.view.makeToast("Proof uploaded!")

                                        self.confirmButton.isHidden = true
                                        self.cancelButton.isHidden = true
                                        self.paymentMethod.isHidden = false
                                        self.uploadButton.isHidden = false

                                    } else {
                                        self.view.hideToastActivity()
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                    }
                                } else {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                    print(response)
                                }
                        }
        }
    }
}

class RequestStepThreeViewController: UIViewController {
    
    var requestData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var priceNotConfirmedView: UILabel!
    @IBOutlet weak var paymentNotConfirmedView: UILabel!
    @IBOutlet weak var paymentConfirmedView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (requestData?.deskripsi[0].nama_barang)! + " " + (requestData?.deskripsi[0].jml)! + " " + (requestData?.deskripsi[0].satuan)!
        
        username.text = requestData?.detail.user_penitip
        date.text = requestData?.detail.tgl_titip
        origin.text = requestData?.detail.asal
        destination.text = requestData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = requestData?.deskripsi[0].deskripsi_barang
        price.text = requestData?.detail.harga ?? "-"
        
        if (requestData?.detail.status_titipan == "Paid, Waiting For Validation") {
            priceNotConfirmedView.backgroundColor = UIColor.lightGray
            priceNotConfirmedView.textColor = UIColor.darkGray
            paymentNotConfirmedView.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            paymentNotConfirmedView.textColor = UIColor.white
            paymentConfirmedView.backgroundColor = UIColor.lightGray
            paymentConfirmedView.textColor = UIColor.darkGray
        } else if (requestData?.detail.status_titipan == "Validated") {
            priceNotConfirmedView.backgroundColor = UIColor.lightGray
            priceNotConfirmedView.textColor = UIColor.darkGray
            paymentNotConfirmedView.backgroundColor = UIColor.lightGray
            paymentNotConfirmedView.textColor = UIColor.darkGray
            paymentConfirmedView.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            paymentConfirmedView.textColor = UIColor.white
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
}


class RequestStepFourViewController: UIViewController {
    
    var requestData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!

    @IBOutlet weak var courier: UILabel!
    @IBOutlet weak var resi: UILabel!
    
    @IBOutlet weak var itemAreBeingTaken: UILabel!
    @IBOutlet weak var itemHasBeenTaken: UILabel!
    @IBOutlet weak var itemAreBeingDelivered: UILabel!
    
    @IBOutlet weak var delivered: UIButton!
    @IBOutlet weak var itemGood: UIButton!
    @IBOutlet weak var itemBroken: UIButton!
    @IBOutlet weak var wrongItem: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (requestData?.deskripsi[0].nama_barang)! + " " + (requestData?.deskripsi[0].jml)! + " " + (requestData?.deskripsi[0].satuan)!
        
        username.text = requestData?.detail.user_penitip
        date.text = requestData?.detail.tgl_titip
        origin.text = requestData?.detail.asal
        destination.text = requestData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = requestData?.deskripsi[0].deskripsi_barang
        price.text = requestData?.detail.harga ?? "-"
        
        courier.text = requestData?.detail.kurir ?? "-"
        resi.text = requestData?.detail.resi ?? "-"
        
        if (requestData?.detail.status_titipan == "Item Is Being Taken") {
            itemAreBeingTaken.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            itemAreBeingTaken.textColor = UIColor.white
            itemHasBeenTaken.backgroundColor = UIColor.lightGray
            itemHasBeenTaken.textColor = UIColor.darkGray
            itemAreBeingDelivered.backgroundColor = UIColor.lightGray
            itemAreBeingDelivered.textColor = UIColor.darkGray
        } else if (requestData?.detail.status_titipan == "Item Has Been Taken") {
            itemAreBeingTaken.backgroundColor = UIColor.lightGray
            itemAreBeingTaken.textColor = UIColor.darkGray
            itemHasBeenTaken.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            itemHasBeenTaken.textColor = UIColor.white
            itemAreBeingDelivered.backgroundColor = UIColor.lightGray
            itemAreBeingDelivered.textColor = UIColor.darkGray
        } else if (requestData?.detail.status_titipan == "Item Is Being Delivered") {
            itemAreBeingTaken.backgroundColor = UIColor.lightGray
            itemAreBeingTaken.textColor = UIColor.darkGray
            itemHasBeenTaken.backgroundColor = UIColor.lightGray
            itemHasBeenTaken.textColor = UIColor.darkGray
            itemAreBeingDelivered.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            itemAreBeingDelivered.textColor = UIColor.white
            
            delivered.isHidden = false
        } else {
            itemAreBeingTaken.backgroundColor = UIColor.lightGray
            itemAreBeingTaken.textColor = UIColor.darkGray
            itemHasBeenTaken.backgroundColor = UIColor.lightGray
            itemHasBeenTaken.textColor = UIColor.darkGray
            itemAreBeingDelivered.backgroundColor = UIColor.lightGray
            itemAreBeingDelivered.textColor = UIColor.darkGray
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func delivered(_ sender: Any) {
        delivered.isHidden = true
        itemGood.isHidden = false
        itemBroken.isHidden = false
        wrongItem.isHidden = false
    }
    
    @IBAction func itemInGoodCondition(_ sender: Any) {
        sendFinalStatus(status: "konfirmasi-barang-diterima")
    }
    
    @IBAction func itemInBrokenCondition(_ sender: Any) {
        sendFinalStatus(status: "konfirmasi-barang-rusak")
    }
    
    @IBAction func wrongItemCondition(_ sender: Any) {
        sendFinalStatus(status: "konfirmasi-barang-salah")
    }
    
    private func sendFinalStatus(status: String) {
        let param = [
            "titipan_id": self.tripId!,
            "token": SessionManager().getUser().token
            ] as Parameters
        
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/" + status,
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Status uploaded!")
                                    self.dismiss(animated: true, completion: nil)
                                    
                                } else {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                }
                            } else {
                                self.view.hideToastActivity()
                                self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                print(response)
                            }
        }
    }
}

class RequestStepFiveViewController: UIViewController {
    
    var requestData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var itemGoodCondition: UILabel!
    @IBOutlet weak var itemWrongCondition: UILabel!
    @IBOutlet weak var itemBrokenCondition: UILabel!
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var feedback: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingView.isHidden = true
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (requestData?.deskripsi[0].nama_barang)! + " " + (requestData?.deskripsi[0].jml)! + " " + (requestData?.deskripsi[0].satuan)!
        
        username.text = requestData?.detail.user_penitip
        date.text = requestData?.detail.tgl_titip
        origin.text = requestData?.detail.asal
        destination.text = requestData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = requestData?.deskripsi[0].deskripsi_barang
        price.text = requestData?.detail.harga ?? "-"
        
        if requestData?.feedback != nil {
            ratingButton.isHidden = true
        }
        
        if (requestData?.detail.status_titipan == "Item Has Been Delivered") {
            itemGoodCondition.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            itemGoodCondition.textColor = UIColor.white
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        } else if (requestData?.detail.status_titipan == "Wrong Item") {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.red
            itemWrongCondition.textColor = UIColor.white
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        } else if (requestData?.detail.status_titipan == "Broken Item") {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.red
            itemBrokenCondition.textColor = UIColor.white
        } else {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showRatingView(sender: AnyObject) {
        ratingView.isHidden = false
        ratingButton.isHidden = true
    }
    
    @IBAction func sendReview(_ sender: Any) {
        self.view.makeToastActivity(.center)
        
        if (feedback.text != nil) {
            let param = [
                "token": SessionManager().getUser().token,
                "from": requestData?.detail.user_perjalanan,
                "to": requestData?.detail.user_penitip,
                "skor": String(ratingStar.totalStars),
                "feed": feedback.text!,
                "titipan_id": tripId!
                ] as Parameters
            
            print(param)
            
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/rate/penitip",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        self.view.makeToast("Feedback send!")
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
                                
            }
        } else {
            self.view.makeToast("Delivery type can't be empty!")
            self.view.hideToastActivity()
        }
    }
}
