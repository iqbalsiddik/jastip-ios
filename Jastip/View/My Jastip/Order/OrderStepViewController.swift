//
//  OrderViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 11/08/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
import Alamofire
import Toast_Swift

class OrderNavViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

class OrderHandlerViewController: UIViewController {
    
    var tripId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkOrder()
    }
    
    private func checkOrder() {
        print(tripId)
        
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/detail/" + tripId + "/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    let loadedOrder = DataJastip(json: json["data"].description)
                                    
                                    self.setupView(loadedOrder: loadedOrder)
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                }
                            } else {
                                self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                print(response)
                            }
                            
        }
    }
    
    private func setupView(loadedOrder: DataJastip) {
        let storyboard = UIStoryboard.init(name: "Order", bundle: Bundle.main)
        let status = loadedOrder.detail.status_titipan
        
        print(status)
        
        if (status == "Unprocessed") {
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderStepOneViewController") as! OrderStepOneViewController
            vc.orderData = loadedOrder
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (status == "Waiting Price Confirmation" || status == "Confirmed, Waiting For Payment" || status == "Paid, Waiting For Validation") {
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderStepTwoViewController") as! OrderStepTwoViewController
            vc.orderData = loadedOrder
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (status == "Validated" || status == "Item Has Been Taken" || status == "Item Is Being Taken") {
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderStepThreeViewController") as! OrderStepThreeViewController
            vc.orderData = loadedOrder
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (status == "Item Is Being Delivered" || status == "Item Has Been Delivered" || status == "Wrong Item" || status == "Broken Item" || status == "Transaction canceled") {
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderStepFourViewController") as! OrderStepFourViewController
            vc.orderData = loadedOrder
            vc.tripId = self.tripId
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class OrderStepOneViewController: UIViewController {
    
    var orderData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var priceProposal: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (orderData?.deskripsi[0].nama_barang)! + " " + (orderData?.deskripsi[0].jml)! + " " + (orderData?.deskripsi[0].satuan)!
        
        username.text = orderData?.detail.user_penitip
        date.text = orderData?.detail.tgl_titip
        origin.text = orderData?.detail.asal
        destination.text = orderData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = orderData?.deskripsi[0].deskripsi_barang
        price.text = orderData?.detail.harga ?? "-"
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTransaction(_ sender: Any) {
        self.view.makeToastActivity(.center)
        
        let param = [
            "titipan_id": tripId!,
            "token": SessionManager().getUser().token,
            ] as Parameters
        
        print(param)
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/pembatalan",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    self.view.makeToast("Cancelled")
                                    self.dismiss(animated: true, completion: nil)
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                }
                            } else {
                                self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                self.view.hideToastActivity()
                                print(response)
                            }
                            
        }
    }
    
    @IBAction func proposePrice(_ sender: Any) {
        self.view.makeToastActivity(.center)
        print(priceProposal.text!)
        
        if (priceProposal.text != nil) {
            let param = [
                "titipan_id": tripId!,
                "token": SessionManager().getUser().token,
                "harga": priceProposal.text!
            ] as Parameters
            
            print(param)
            
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/upload-harga",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        self.view.makeToast("Price saved!")
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
                                
            }
        } else {
            self.view.makeToast("Price can't be empty!")
            self.view.hideToastActivity()
        }
    }
}

class OrderStepTwoViewController: UIViewController {
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var priceNotConfirmedView: UILabel!
    @IBOutlet weak var paymentNotConfirmedView: UILabel!
    @IBOutlet weak var paymentConfirmedView: UILabel!
    
    var orderData: DataJastip?
    var tripId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (orderData?.deskripsi[0].nama_barang)! + " " + (orderData?.deskripsi[0].jml)! + " " + (orderData?.deskripsi[0].satuan)!
        
        username.text = orderData?.detail.user_penitip
        date.text = orderData?.detail.tgl_titip
        origin.text = orderData?.detail.asal
        destination.text = orderData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = orderData?.deskripsi[0].deskripsi_barang
        price.text = orderData?.detail.harga ?? "-"
        
        if (orderData?.detail.status_titipan == "Waiting Price Confirmation") {
            priceNotConfirmedView.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            priceNotConfirmedView.textColor = UIColor.white
            paymentNotConfirmedView.backgroundColor = UIColor.lightGray
            paymentNotConfirmedView.textColor = UIColor.darkGray
            paymentConfirmedView.backgroundColor = UIColor.lightGray
            paymentConfirmedView.textColor = UIColor.darkGray
        } else if (orderData?.detail.status_titipan == "Paid, Waiting For Validation") {
            priceNotConfirmedView.backgroundColor = UIColor.lightGray
            priceNotConfirmedView.textColor = UIColor.darkGray
            paymentNotConfirmedView.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            paymentNotConfirmedView.textColor = UIColor.white
            paymentConfirmedView.backgroundColor = UIColor.lightGray
            paymentConfirmedView.textColor = UIColor.darkGray
        } else if (orderData?.detail.status_titipan == "Confirmed, Waiting For Payment") {
            priceNotConfirmedView.backgroundColor = UIColor.lightGray
            priceNotConfirmedView.textColor = UIColor.darkGray
            paymentNotConfirmedView.backgroundColor = UIColor.lightGray
            paymentNotConfirmedView.textColor = UIColor.darkGray
            paymentConfirmedView.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            paymentConfirmedView.textColor = UIColor.white
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
}

class OrderStepThreeViewController: UIViewController {
    
    var orderData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var courierView: UIView!
    @IBOutlet weak var takenButton: UIButton!
    @IBOutlet weak var courier: UITextField!
    @IBOutlet weak var resi: UITextField!
    
    private var deliveryType: String = "Direct"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deliveryView.isHidden = true
        courierView.isHidden = true
        
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (orderData?.deskripsi[0].nama_barang)! + " " + (orderData?.deskripsi[0].jml)! + " " + (orderData?.deskripsi[0].satuan)!
        
        username.text = orderData?.detail.user_penitip
        date.text = orderData?.detail.tgl_titip
        origin.text = orderData?.detail.asal
        destination.text = orderData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = orderData?.deskripsi[0].deskripsi_barang
        price.text = orderData?.detail.harga ?? "-"
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }

    @IBAction func showDeliveryView(sender: AnyObject) {
        deliveryView.isHidden = false
        takenButton.isHidden = true
    }
    
    @IBAction func radioDirect(sender: AnyObject) {
        deliveryType = "Direct"
        courierView.isHidden = true
    }
    
    @IBAction func radioCourier(sender: AnyObject) {
        deliveryType = "Courier"
        courierView.isHidden = false
    }
    
    @IBAction func sendItem(_ sender: Any) {
        self.view.makeToastActivity(.center)
        
        if (deliveryType != nil) {
            let param = [
                "token": SessionManager().getUser().token,
                "kurir": deliveryType,
                "resi": resi.text!,
                "titipan_id": tripId!
                ] as Parameters
            
            print(param)
            
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/titipan-sedang-diantar",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        self.view.makeToast("Delivery saved!")
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
                                
            }
        } else {
            self.view.makeToast("Delivery type can't be empty!")
            self.view.hideToastActivity()
        }
    }
}

class OrderStepFourViewController: UIViewController {
    
    var orderData: DataJastip?
    var tripId: String?
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var itemGoodCondition: UILabel!
    @IBOutlet weak var itemWrongCondition: UILabel!
    @IBOutlet weak var itemBrokenCondition: UILabel!
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var feedback: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingView.isHidden = true
        setupBarButton()
        setupView()
    }
    
    private func setupBarButton() {
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    private func setupView() {
        let namaBarang = (orderData?.deskripsi[0].nama_barang)! + " " + (orderData?.deskripsi[0].jml)! + " " + (orderData?.deskripsi[0].satuan)!
        
        username.text = orderData?.detail.user_penitip
        date.text = orderData?.detail.tgl_titip
        origin.text = orderData?.detail.asal
        destination.text = orderData?.detail.tujuan
        itemName.text = namaBarang
        itemDesc.text = orderData?.deskripsi[0].deskripsi_barang
        price.text = orderData?.detail.harga ?? "-"
        
        if orderData?.feedback != nil {
            ratingButton.isHidden = true
        }
        
        if (orderData?.detail.status_titipan == "Item Has Been Delivered") {
            itemGoodCondition.backgroundColor = UIColor.init(red: 81, green: 139, blue: 227)
            itemGoodCondition.textColor = UIColor.white
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        } else if (orderData?.detail.status_titipan == "Wrong Item") {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.red
            itemWrongCondition.textColor = UIColor.white
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        } else if (orderData?.detail.status_titipan == "Broken Item") {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.red
            itemBrokenCondition.textColor = UIColor.white
        } else {
            itemGoodCondition.backgroundColor = UIColor.lightGray
            itemGoodCondition.textColor = UIColor.darkGray
            itemWrongCondition.backgroundColor = UIColor.lightGray
            itemWrongCondition.textColor = UIColor.darkGray
            itemBrokenCondition.backgroundColor = UIColor.lightGray
            itemBrokenCondition.textColor = UIColor.darkGray
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showRatingView(sender: AnyObject) {
        ratingView.isHidden = false
        ratingButton.isHidden = true
    }
    
    @IBAction func sendReview(_ sender: Any) {
        self.view.makeToastActivity(.center)
        
        if (feedback.text != nil) {
            let param = [
                "token": SessionManager().getUser().token,
                "from": orderData?.detail.user_perjalanan,
                "to": orderData?.detail.user_penitip,
                "skor": String(ratingStar.totalStars),
                "feed": feedback.text!,
                "titipan_id": tripId!
                ] as Parameters
            
            print(param)
            
            Alamofire.request(StaticURL.API_BASE_URL + "titipan/rate/penitip",
                              method: .post,
                              parameters: param,
                              encoding: JSONEncoding.default).responseJSON { response in
                                if (response.result.value != nil) {
                                    let json = JSON(response.result.value!)
                                    if json["status_code"].intValue == 200 {
                                        self.view.makeToast("Feedback send!")
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                        self.view.hideToastActivity()
                                    }
                                } else {
                                    self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                    print(response)
                                }
                                
            }
        } else {
            self.view.makeToast("Delivery type can't be empty!")
            self.view.hideToastActivity()
        }
    }
}
