//
//  RequestDetailViewModel.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 03/09/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import Toast_Swift

class RequestDetailNavViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

class RequestDetailViewController: UIViewController {
    
    var selectedRequest: DataJastip?
    var tripId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkOrder()
    }
    
    private func checkOrder() {
        self.view.makeToastActivity(.center)
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/detail/" + tripId + "/" + SessionManager().getUser().token,
                          method: .get,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    let loadedOrder = DataJastip(json: json["data"].description)
                                    
                                    self.loadTrip(loadedOrder: loadedOrder)
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                }
                            } else {
                                self.view.makeToast("Can't do the request now! Please try again", duration: 3.0, position: .bottom)
                                print(response)
                            }
                            
        }
    }
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var arrival: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var avatar2: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemUnit: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    
    
    private func loadTrip(loadedOrder: DataJastip) {
        username.text = loadedOrder.detail.user_penitip
        origin.text = loadedOrder.detail.asal
        arrival.text = loadedOrder.detail.tujuan
        desc.text = "Deadline at " + loadedOrder.detail.tgl_selesai
        
        itemName.text = loadedOrder.deskripsi[0].nama_barang
        itemUnit.text = loadedOrder.deskripsi[0].jml + " " + loadedOrder.deskripsi[0].satuan
        itemDesc.text = loadedOrder.deskripsi[0].deskripsi_barang
        
        
        avatar.setRounded()
        avatar.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + loadedOrder.detail.photo)!)
        avatar2.af_setImage(withURL: URL(string: StaticURL.FOTO_URL + loadedOrder.detail.photo)!)
        
        self.view.hideToastActivity()
    }
    
    @IBAction func backToList() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func takeRequest(_ sender: Any) {
        let param = [
            "titipan_id": tripId,
            "user_username": SessionManager().getUser().username,
            "token": SessionManager().getUser().token
            ] as Parameters
        
        Alamofire.request(StaticURL.API_BASE_URL + "titipan/ambil-request",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default).responseJSON { response in
                            if (response.result.value != nil) {
                                let json = JSON(response.result.value!)
                                if json["status_code"].intValue == 200 {
                                    self.view.makeToast("Request taken!")
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.pindahStoryboard()
                                    }
                                } else {
                                    self.view.makeToast("Something went wrong! Please try again", duration: 3.0, position: .bottom)
                                    self.view.hideToastActivity()
                                }
                            } else {
                                self.view.makeToast(response.error?.localizedDescription, duration: 3.0, position: .bottom)
                                self.view.hideToastActivity()
                                print(response)
                            }
                            
        }
    }
    
    func pindahStoryboard() {
        let storyboard = UIStoryboard(name: "Order", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OrderNavViewController") as! OrderNavViewController
        let orderHandlerViewController = viewController.viewControllers.first as! OrderHandlerViewController
        orderHandlerViewController.tripId = self.tripId
        
        present(viewController, animated: true, completion: nil)
    }
}

