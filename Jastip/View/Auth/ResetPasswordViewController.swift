//
//  ResetStepOneViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 01/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class ResetStepOneViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backToLogin() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var email: UITextField!
    
    @IBAction func gotoForgetStepTwo() {
        if email.text?.isEmpty ?? true {
            self.view.makeToast("Please input your email!")
        } else if !(email.text?.isValidEmail())! {
            self.view.makeToast("Please input a valid email!")
        } else {
            self.view.makeToastActivity(.center)
            
            Alamofire.request(StaticURL.API_BASE_URL + "forgot-password",
                              method: .post,
                              parameters: ["key": email.text!],
                              encoding: JSONEncoding.default)
                .responseJSON { (response) -> Void in
                    if response.result.value != nil {
                        let json = JSON(response.result.value!)
                        if json["status_code"].intValue == 200 {
                            self.view.hideToastActivity()
                            self.gotoSecondStep()
                        } else if json["status_code"].intValue == 403 {
                            self.view.hideToastActivity()
                            self.view.makeToast("Email not found. Make sure you input your registered email")
                        } else {
                            self.view.hideToastActivity()
                            self.view.makeToast(json["message"].description)
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(response.error?.localizedDescription)
                    }
            }
        }
    }
    
    private func gotoSecondStep() {
        self.performSegue(withIdentifier: "gotoForgetStepTwo", sender: (Any).self)
    }
}

class ResetStepTwoViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var otp: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var confirmPass: UITextField!
    
    @IBAction func resetPassword() {
        if checkValue() {
            self.view.makeToastActivity(.center)
            
            Alamofire.request(StaticURL.API_BASE_URL + "reset-password",
                              method: .post,
                              parameters: ["username": username.text!,
                                           "new_password": pass.text!,
                                           "otp": otp.text!],
                              encoding: JSONEncoding.default)
                .responseJSON { (response) -> Void in
                    let json = JSON(response.result.value!)
                    if json["status_code"].intValue == 200 {
                        self.view.hideToastActivity()
                        self.view.makeToast("Reset profile success!", duration: 2.0, position: .bottom)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.dismiss(animated: true, completion: nil)
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast(json["message"].description)
                    }
            }
        }
    }
    
    private func checkValue() -> Bool {
        if (username.text?.isEmpty)! {
            self.view.makeToast("Please input your username")
            return false
        }
        
        if (pass.text?.isEmpty)! {
            self.view.makeToast("Please input your password")
            return false
        }
        
        if (confirmPass.text?.isEmpty)! {
            self.view.makeToast("Please input your confirm password")
            return false
        }
        
        if (pass.text! != confirmPass.text!) {
            self.view.makeToast("Password and confirm password must be the same")
            return false
        }
        
        if (otp.text?.isEmpty)! {
            self.view.makeToast("Please input your OTP")
            return false
        }
        
        return true
    }
}


