//
//  LoginViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 30/06/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class LoginViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerText: UILabel!
    @IBOutlet weak var forgetPasswordText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        let forgetPasswordGesture = UITapGestureRecognizer(target: self, action: #selector(gotoForgetPassword))
        forgetPasswordText.addGestureRecognizer(forgetPasswordGesture)
        
        let registerGesture = UITapGestureRecognizer(target: self, action: #selector(gotoRegister))
        registerText.addGestureRecognizer(registerGesture)
    }
    
    //MARK: Action
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        let username: String = usernameTextField.text!
        let password: String = passwordTextField.text!
        
        doLogin(username: username, password: password)
    }
    

    @objc private func gotoRegister() {
        self.performSegue(withIdentifier: "gotoRegister", sender: registerText)
    }
    
    @objc private func gotoForgetPassword() {
        self.performSegue(withIdentifier: "gotoForgetPassword", sender: forgetPasswordText)
    }
    
    private func doLogin(username: String, password: String) {
        startAnimating()
        Alamofire.request(StaticURL.API_BASE_URL + "login",
                          method: .post,
                          parameters: ["username": username,
                                       "password": password],
                          encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                let json = JSON(response.result.value!)
                if json["status_code"].intValue == 200 {
                    let user = User(json: json["data"].description)
                    SessionManager().setUser(user: user)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        self.pindahStoryboard(user: user)
                        
                        self.stopAnimating()
                    }
                } else {
                    self.view.makeToast("Login failed! Please check your username/password")
                    
                    self.stopAnimating()
                }
        }
    }
    
    func pindahStoryboard(user: User) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        present(viewController, animated: true, completion: nil)
    }
}
