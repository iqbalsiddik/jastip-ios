//
//  RegisterViewController.swift
//  Jastip
//
//  Created by Ganendra Afrasya on 01/07/19.
//  Copyright © 2019 gsculerlor. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toast_Swift
import Alamofire

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    private var avatarStr: String?
    private var ktpStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        setupView()
    }
    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -150 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }


    private func setupView() {
        avatar.setRounded()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showImagePicker(sender:)))
        avatar.addGestureRecognizer(tap)
        avatar.isUserInteractionEnabled = true
    }
    
    @IBAction func backToLogin() {
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func register() {
        if checkValue() {
            self.view.makeToastActivity(.center)
            Alamofire.request(
                StaticURL.API_BASE_URL + "register",
                method: .post,
                parameters: [
                    "username": usernameTextField.text!,
                    "password": passwordTextField.text!,
                    "konfirmasi password" : confirmPasswordTextField.text!,
                    "email": emailTextField.text!,
                    "nama_depan": firstnameTextField.text!,
                    "nama_blkg" : lastnameTextField.text!,
                    "nohp": phoneNumberTextField.text!,
                    "photo": avatarStr!,
                    "ktp" : ktpStr!,
                    ] as Parameters,
                encoding: JSONEncoding.default)
                .responseJSON { (response) -> Void in
//                    print(response)
                    if response.result.value != nil {
                        let json = JSON(response.result.value!)
                        if (response.response?.statusCode == 200) {
                            self.view.makeToast("Account created!")
                            self.dismiss(animated: true, completion: nil)
                        } else if (json["message"].description.contains("exist")) {
                            print(json["message"])
                            self.view.makeToast("Error: username already taken")
                        } else if (response.response?.statusCode == 500) {
                            print(json["message"])
                            self.view.makeToast("Error: email already taken")
                        } else {
                            print(json["message"])
                            self.view.makeToast("Error: " + json["message"].description)
                        }
                    } else {
                        self.view.hideToastActivity()
                        self.view.makeToast("Timeout")
                    }
            }
        }
    }
    
    private func checkValue() -> Bool {
        if (usernameTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Username can't be empty!")
            return false
        }
        
        if (usernameTextField.text!.count < 6) {
            self.view.makeToast("Username can't less than 6 digit value")
            return false
        }
        
        if (firstnameTextField.text!.isEmpty as Bool) {
            self.view.makeToast("First name can't be empty!")
            return false
        }
        
        if (lastnameTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Last name can't be empty!")
            return false
        }
        
        if (emailTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Email can't be empty!")
            return false
        }
        
        if (!(emailTextField.text?.isValidEmail())!) {
            self.view.makeToast("Unknown email format")
            return false
        }
        
        if (passwordTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Password can't be empty!")
            return false
        }
        
        if (confirmPasswordTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Confirm password can't be empty!")
            return false
        }
        
        if (confirmPasswordTextField.text!.count < 6 || passwordTextField.text!.count < 6) {
            self.view.makeToast("Password can't less than 6 digit value")
            return false
        }
        
        if (!passwordTextField.text!.elementsEqual(confirmPasswordTextField.text!)) {
            self.view.makeToast("Not same!")
            return false
        }
        
        if (phoneNumberTextField.text!.isEmpty as Bool) {
            self.view.makeToast("Phone number can't be empty!")
            return false
        }
        
        if (phoneNumberTextField.text!.count < 10 || phoneNumberTextField.text!.count > 13) {
            self.view.makeToast("Can't less than 10 digits or more than 13 digits")
            return false
        }
        
        return true
    }
    
    @IBAction func showImagePickerKtp() {
        ImagePickerManager().pickImage(self) { image in
            self.ktpStr = "data:image/png;base64,[" + (image
                .compressTo(5)!
                .toBase64())! + "]"
            
        }
        
    }
    
    var imagePicker = UIImagePickerController()
    
    @objc func showImagePicker(sender: AnyObject) {
        ImagePickerManager().pickImage(self) { image in
            self.avatarStr =  "data:image/png;base64,[" + (image.compressTo(5)!.toBase64())! + "]"
            //            print(self.avatar!)
            self.avatar.image = image
        }
        
    }
}
